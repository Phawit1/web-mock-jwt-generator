const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const jwt = require("jsonwebtoken");

const app = express();

app.use(express.static(path.join(__dirname, "")));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.post("/jwtGenerate", async (req, res) => {
  const { fullname, roles, rmid, companyid, userid, productid, expiredtime } = req.body;

  const token = jwt.sign({ fullname, roles, rmid, companyid, userid, productid }, "secretKey", {
    noTimestamp: true,
    expiresIn: `${expiredtime}s`,
  });

  console.log("token", token);
  res.status(200).send({ token });
});

app.listen(4000, () => {
  console.log("server start at port 4000");
});
